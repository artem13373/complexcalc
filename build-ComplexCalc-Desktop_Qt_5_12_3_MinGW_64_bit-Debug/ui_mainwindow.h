/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *real;
    QLabel *img;
    QLabel *label_2;
    QLabel *label;
    QLabel *label_4;
    QPushButton *calc92;
    QPushButton *deg;
    QPushButton *calc31;
    QPushButton *mult;
    QPushButton *calc52;
    QPushButton *calc01;
    QPushButton *calc02;
    QPushButton *sqr;
    QPushButton *calcSign1;
    QPushButton *sum;
    QPushButton *calc51;
    QPushButton *minus;
    QPushButton *calc22;
    QPushButton *calc21;
    QPushButton *calc71;
    QPushButton *calcDot1;
    QPushButton *calc82;
    QPushButton *calcDot2;
    QPushButton *calc41;
    QPushButton *calc61;
    QPushButton *calc11;
    QPushButton *calc12;
    QPushButton *calc62;
    QPushButton *calcSign2;
    QPushButton *calc42;
    QPushButton *calc72;
    QPushButton *calc91;
    QPushButton *calc32;
    QPushButton *pushButton_2;
    QPushButton *pushButton;
    QPushButton *calc81;
    QPushButton *div;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(611, 348);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        real = new QLabel(centralWidget);
        real->setObjectName(QString::fromUtf8("real"));
        real->setGeometry(QRect(0, 0, 91, 61));
        QFont font;
        font.setPointSize(20);
        real->setFont(font);
        img = new QLabel(centralWidget);
        img->setObjectName(QString::fromUtf8("img"));
        img->setGeometry(QRect(110, 10, 111, 41));
        img->setFont(font);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(90, 0, 21, 51));
        label_2->setFont(font);
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(260, 10, 261, 41));
        label->setFont(font);
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(220, 0, 21, 61));
        label_4->setFont(font);
        calc92 = new QPushButton(centralWidget);
        calc92->setObjectName(QString::fromUtf8("calc92"));
        calc92->setGeometry(QRect(170, 50, 31, 31));
        deg = new QPushButton(centralWidget);
        deg->setObjectName(QString::fromUtf8("deg"));
        deg->setGeometry(QRect(260, 50, 31, 31));
        calc31 = new QPushButton(centralWidget);
        calc31->setObjectName(QString::fromUtf8("calc31"));
        calc31->setGeometry(QRect(60, 110, 31, 31));
        mult = new QPushButton(centralWidget);
        mult->setObjectName(QString::fromUtf8("mult"));
        mult->setGeometry(QRect(260, 80, 31, 31));
        calc52 = new QPushButton(centralWidget);
        calc52->setObjectName(QString::fromUtf8("calc52"));
        calc52->setGeometry(QRect(140, 80, 31, 31));
        calc01 = new QPushButton(centralWidget);
        calc01->setObjectName(QString::fromUtf8("calc01"));
        calc01->setGeometry(QRect(30, 140, 31, 31));
        calc02 = new QPushButton(centralWidget);
        calc02->setObjectName(QString::fromUtf8("calc02"));
        calc02->setGeometry(QRect(140, 140, 31, 31));
        sqr = new QPushButton(centralWidget);
        sqr->setObjectName(QString::fromUtf8("sqr"));
        sqr->setGeometry(QRect(320, 80, 31, 31));
        calcSign1 = new QPushButton(centralWidget);
        calcSign1->setObjectName(QString::fromUtf8("calcSign1"));
        calcSign1->setGeometry(QRect(60, 140, 31, 31));
        sum = new QPushButton(centralWidget);
        sum->setObjectName(QString::fromUtf8("sum"));
        sum->setGeometry(QRect(290, 50, 31, 31));
        calc51 = new QPushButton(centralWidget);
        calc51->setObjectName(QString::fromUtf8("calc51"));
        calc51->setGeometry(QRect(30, 80, 31, 31));
        minus = new QPushButton(centralWidget);
        minus->setObjectName(QString::fromUtf8("minus"));
        minus->setGeometry(QRect(320, 50, 31, 31));
        calc22 = new QPushButton(centralWidget);
        calc22->setObjectName(QString::fromUtf8("calc22"));
        calc22->setGeometry(QRect(140, 110, 31, 31));
        calc21 = new QPushButton(centralWidget);
        calc21->setObjectName(QString::fromUtf8("calc21"));
        calc21->setGeometry(QRect(30, 110, 31, 31));
        calc71 = new QPushButton(centralWidget);
        calc71->setObjectName(QString::fromUtf8("calc71"));
        calc71->setGeometry(QRect(0, 50, 31, 31));
        calcDot1 = new QPushButton(centralWidget);
        calcDot1->setObjectName(QString::fromUtf8("calcDot1"));
        calcDot1->setGeometry(QRect(0, 140, 31, 31));
        calc82 = new QPushButton(centralWidget);
        calc82->setObjectName(QString::fromUtf8("calc82"));
        calc82->setGeometry(QRect(140, 50, 31, 31));
        calcDot2 = new QPushButton(centralWidget);
        calcDot2->setObjectName(QString::fromUtf8("calcDot2"));
        calcDot2->setGeometry(QRect(110, 140, 31, 31));
        calc41 = new QPushButton(centralWidget);
        calc41->setObjectName(QString::fromUtf8("calc41"));
        calc41->setGeometry(QRect(0, 80, 31, 31));
        calc61 = new QPushButton(centralWidget);
        calc61->setObjectName(QString::fromUtf8("calc61"));
        calc61->setGeometry(QRect(60, 80, 31, 31));
        calc11 = new QPushButton(centralWidget);
        calc11->setObjectName(QString::fromUtf8("calc11"));
        calc11->setGeometry(QRect(0, 110, 31, 31));
        calc12 = new QPushButton(centralWidget);
        calc12->setObjectName(QString::fromUtf8("calc12"));
        calc12->setGeometry(QRect(110, 110, 31, 31));
        calc62 = new QPushButton(centralWidget);
        calc62->setObjectName(QString::fromUtf8("calc62"));
        calc62->setGeometry(QRect(170, 80, 31, 31));
        calcSign2 = new QPushButton(centralWidget);
        calcSign2->setObjectName(QString::fromUtf8("calcSign2"));
        calcSign2->setGeometry(QRect(170, 140, 31, 31));
        calc42 = new QPushButton(centralWidget);
        calc42->setObjectName(QString::fromUtf8("calc42"));
        calc42->setGeometry(QRect(110, 80, 31, 31));
        calc72 = new QPushButton(centralWidget);
        calc72->setObjectName(QString::fromUtf8("calc72"));
        calc72->setGeometry(QRect(110, 50, 31, 31));
        calc91 = new QPushButton(centralWidget);
        calc91->setObjectName(QString::fromUtf8("calc91"));
        calc91->setGeometry(QRect(60, 50, 31, 31));
        calc32 = new QPushButton(centralWidget);
        calc32->setObjectName(QString::fromUtf8("calc32"));
        calc32->setGeometry(QRect(170, 110, 31, 31));
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setGeometry(QRect(320, 110, 31, 31));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(260, 110, 91, 31));
        calc81 = new QPushButton(centralWidget);
        calc81->setObjectName(QString::fromUtf8("calc81"));
        calc81->setGeometry(QRect(30, 50, 31, 31));
        div = new QPushButton(centralWidget);
        div->setObjectName(QString::fromUtf8("div"));
        div->setGeometry(QRect(290, 80, 31, 31));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 611, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        real->setText(QApplication::translate("MainWindow", "0", nullptr));
        img->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_2->setText(QApplication::translate("MainWindow", ";", nullptr));
        label->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "i", nullptr));
        calc92->setText(QApplication::translate("MainWindow", "9", nullptr));
        deg->setText(QApplication::translate("MainWindow", "^", nullptr));
        calc31->setText(QApplication::translate("MainWindow", "3", nullptr));
        mult->setText(QApplication::translate("MainWindow", "*", nullptr));
        calc52->setText(QApplication::translate("MainWindow", "5", nullptr));
        calc01->setText(QApplication::translate("MainWindow", "0", nullptr));
        calc02->setText(QApplication::translate("MainWindow", "0", nullptr));
        sqr->setText(QApplication::translate("MainWindow", "sqrt", nullptr));
        calcSign1->setText(QApplication::translate("MainWindow", "+/-", nullptr));
        sum->setText(QApplication::translate("MainWindow", "+", nullptr));
        calc51->setText(QApplication::translate("MainWindow", "5", nullptr));
        minus->setText(QApplication::translate("MainWindow", "-", nullptr));
        calc22->setText(QApplication::translate("MainWindow", "2", nullptr));
        calc21->setText(QApplication::translate("MainWindow", "2", nullptr));
        calc71->setText(QApplication::translate("MainWindow", "7", nullptr));
        calcDot1->setText(QApplication::translate("MainWindow", ".", nullptr));
        calc82->setText(QApplication::translate("MainWindow", "8", nullptr));
        calcDot2->setText(QApplication::translate("MainWindow", ".", nullptr));
        calc41->setText(QApplication::translate("MainWindow", "4", nullptr));
        calc61->setText(QApplication::translate("MainWindow", "6", nullptr));
        calc11->setText(QApplication::translate("MainWindow", "1", nullptr));
        calc12->setText(QApplication::translate("MainWindow", "1", nullptr));
        calc62->setText(QApplication::translate("MainWindow", "6", nullptr));
        calcSign2->setText(QApplication::translate("MainWindow", "+/-", nullptr));
        calc42->setText(QApplication::translate("MainWindow", "4", nullptr));
        calc72->setText(QApplication::translate("MainWindow", "7", nullptr));
        calc91->setText(QApplication::translate("MainWindow", "9", nullptr));
        calc32->setText(QApplication::translate("MainWindow", "3", nullptr));
        pushButton_2->setText(QApplication::translate("MainWindow", "AC", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "=", nullptr));
        calc81->setText(QApplication::translate("MainWindow", "8", nullptr));
        div->setText(QApplication::translate("MainWindow", "/", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
