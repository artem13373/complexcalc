#ifndef TST_COMPLEX_H
#define TST_COMPLEX_H

#include <QtCore>
#include <QtTest/QtTest>


class Test_Complex : public QObject
{
    Q_OBJECT

public:
    Test_Complex();

private slots:
    void test_sum();
    void test_minus();
    void test_mult();
    void test_div();
    void test_deg();
    void test_sqrt();

};

#endif // TST_COMPLEX_H
