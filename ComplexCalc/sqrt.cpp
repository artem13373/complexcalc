#include "functions.h"
#include <qmath.h>

QString sqrt (double a, double b, int n)
{
    double real = 0;
    double img = 0;
    double sq = sqrt(qPow(a,2)+qPow(b,2));
    QString s = "";
    if (a>0)
    {
         real = (qPow(sq,n) * cos(atan(b/a)/n));
         img = (qPow(sq,n) * sin(atan(b/a)/n));
    }
    if (a<0)
    {
         real = (qPow(sq,n) * cos((atan(b/a)+M_PI)/n));
         img = (qPow(sq,n) * sin(atan(b/a)+M_PI)/n);
    }

    s = QString("%1;%2i")
            .arg(real).arg(img);
    return s;
}
