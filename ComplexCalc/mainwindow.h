#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private slots:
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;

private slots:
    void on_calcDot1_clicked();

    void on_calcDot2_clicked();

    void on_pushButton_2_clicked();

    void digit_numbers();

    void digit_img();

    void sign1();

    void sign2();

    void math_operations();

    void pushAC();

};

#endif // MAINWINDOW_H*/
