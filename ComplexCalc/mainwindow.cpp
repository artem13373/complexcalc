#include "mainwindow.h"
#include "functions.h"
#include "ui_mainwindow.h"

double real1;
double img1;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->calc01, SIGNAL(clicked()), this, SLOT(digit_numbers()));
    connect(ui->calc11, SIGNAL(clicked()), this, SLOT(digit_numbers()));
    connect(ui->calc21, SIGNAL(clicked()), this, SLOT(digit_numbers()));
    connect(ui->calc31, SIGNAL(clicked()), this, SLOT(digit_numbers()));
    connect(ui->calc41, SIGNAL(clicked()), this, SLOT(digit_numbers()));
    connect(ui->calc51, SIGNAL(clicked()), this, SLOT(digit_numbers()));
    connect(ui->calc61, SIGNAL(clicked()), this, SLOT(digit_numbers()));
    connect(ui->calc71, SIGNAL(clicked()), this, SLOT(digit_numbers()));
    connect(ui->calc81, SIGNAL(clicked()), this, SLOT(digit_numbers()));
    connect(ui->calc91, SIGNAL(clicked()), this, SLOT(digit_numbers()));

    connect(ui->calc02, SIGNAL(clicked()), this, SLOT(digit_img()));
    connect(ui->calc12, SIGNAL(clicked()), this, SLOT(digit_img()));
    connect(ui->calc22, SIGNAL(clicked()), this, SLOT(digit_img()));
    connect(ui->calc32, SIGNAL(clicked()), this, SLOT(digit_img()));
    connect(ui->calc42, SIGNAL(clicked()), this, SLOT(digit_img()));
    connect(ui->calc52, SIGNAL(clicked()), this, SLOT(digit_img()));
    connect(ui->calc62, SIGNAL(clicked()), this, SLOT(digit_img()));
    connect(ui->calc72, SIGNAL(clicked()), this, SLOT(digit_img()));
    connect(ui->calc82, SIGNAL(clicked()), this, SLOT(digit_img()));
    connect(ui->calc92, SIGNAL(clicked()), this, SLOT(digit_img()));

    connect(ui->calcSign1, SIGNAL(clicked()), this, SLOT(sign1()));

    connect(ui->calcSign2, SIGNAL(clicked()), this, SLOT(sign2()));

    connect(ui->sum, SIGNAL(clicked()), this, SLOT(math_operations()));
    connect(ui->minus, SIGNAL(clicked()), this, SLOT(math_operations()));
    connect(ui->mult, SIGNAL(clicked()), this, SLOT(math_operations()));
    connect(ui->div, SIGNAL(clicked()), this, SLOT(math_operations()));
    connect(ui->deg, SIGNAL(clicked()), this, SLOT(math_operations()));
    connect(ui->sqr, SIGNAL(clicked()), this, SLOT(math_operations()));
    ui->sum->setCheckable(true);
    ui->minus->setCheckable(true);
    ui->mult->setCheckable(true);
    ui->div->setCheckable(true);
    ui->deg->setCheckable(true);
    ui->sqr->setCheckable(true);

    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(pushAC()));
    ui->pushButton_2->setCheckable(true);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::digit_numbers()
{
    QPushButton *button = (QPushButton*)sender();

    double all_numbers;
    QString new_label;
    all_numbers = (ui->real->text() + button->text()).toDouble();
    new_label = QString::number(all_numbers);

    ui->real->setText(new_label);
}

void MainWindow::digit_img()
{
    QPushButton *button = (QPushButton*)sender();

    double all_numbers;
    QString new_label;
    all_numbers = (ui->img->text() + button->text()).toDouble();
    new_label = QString::number(all_numbers);

    ui->img->setText(new_label);
}

void MainWindow::on_calcDot1_clicked()
{
    if(!(ui->real->text().contains('.')))
     ui->real->setText(ui->real->text() + ".");
}

void MainWindow::on_calcDot2_clicked()
{
    if(!(ui->img->text().contains('.')))
     ui->img->setText(ui->img->text() + ".");
}

void MainWindow::sign1()
{
    QPushButton *button = (QPushButton*)sender();

    double all_numbers;
    QString new_label;

    if (button->text()=="+/-"){
    all_numbers = (ui->real->text()).toDouble();
    all_numbers = all_numbers*(-1);
    new_label = QString::number(all_numbers);

    ui->real->setText(new_label);}
}

void MainWindow::sign2()
{
    QPushButton *button = (QPushButton*)sender();

    double all_numbers;
    QString new_label;

    if (button->text()=="+/-"){
    all_numbers = (ui->img->text()).toDouble();
    all_numbers = all_numbers*(-1);
    new_label = QString::number(all_numbers);

    ui->img->setText(new_label);}
}

void MainWindow::math_operations()
{
    QPushButton *button = (QPushButton*)sender();

    real1 = ui->real->text().toDouble();
    img1 = ui->img->text().toDouble();
    ui->real->setText("0");
    ui->img->setText("0");

    button->setChecked(true);
}

void MainWindow::on_pushButton_2_clicked()
{

}

void MainWindow::on_pushButton_clicked()
{


    QString str = "";
    double real2;
    double img2;

    real2 = ui->real->text().toDouble();
    img2 = ui->img->text().toDouble();

    if (ui->sum->isChecked())
    {
        str = sum(real1, img1, real2, img2);
        ui->sum->setChecked(false);

    }else  if (ui->minus->isChecked())
    {
        str = minus(real1, img1, real2, img2);
        ui->minus->setChecked(false);
    }else  if (ui->mult->isChecked())
    {
        str = mult(real1, img1, real2, img2);
        ui->mult->setChecked(false);
    }else  if (ui->div->isChecked())
    {
        str = div(real1, img1, real2, img2);
        ui->div->setChecked(false);
    }else  if (ui->deg->isChecked())
    {
        str = deg(real1, img1, real2);
        ui->deg->setChecked(false);
    }else  if (ui->sqr->isChecked())
    {
        str = sqrt(real1, img1, real2);
        ui->sqr->setChecked(false);
    }
    ui->label->setText(str);
    ui->real->setText("0");
    ui->img->setText("0");
}

void MainWindow::pushAC()
{
    ui->real->setText("0");
    ui->img->setText("0");
    ui->pushButton_2->setChecked(false);
}
